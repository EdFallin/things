import {Component, OnInit} from '@angular/core';
import {Room} from './rooms/Room';
import {Place} from './places/Place';
import {Thing} from './things/Thing';
import {ThingService} from './thing-service/thing.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Things';

  // initialized empty so conditionals in template can work right from the beginning
  rooms: Array<Room> = [];
  places: Array<Place> = [];
  things: Array<Thing> = [];

  private room: Room;
  private place: Place;

  constructor(private thingService: ThingService) {
  }

  ngOnInit() {
    this.thingService.getRooms()
      .subscribe((rooms) => {
        this.rooms = rooms;
      });
  }

  whenRoomChosen(room: Room) {
    this.thingService.getPlacesInRoom(room)
      .subscribe((places) => {
        this.places = places;
      });

    this.things = [];  // until a place is chosen

    /* ++ switch to an Angular approach, like ViewChild or similar, if possible */
    this.room = room;  // for choosing things
  }

  whenPlaceChosen(place: Place) {
    this.thingService.getThingsInPlaceInRoom(this.room, place)
      .subscribe((things) => {
        this.things = things;
      });

    /* ++ switch to an Angular approach, like ViewChild or similar, if possible */
    this.place = place;  // for choosing things
  }
}
