import {Component, OnInit, EventEmitter, Input, Output} from '@angular/core';
import {Place} from '../Place';

@Component({
  selector: 'app-place',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.css']
})
export class PlaceComponent implements OnInit {
  @Input() place: Place;

  @Output() placeChosen: EventEmitter<Place>;


  constructor() {
    this.placeChosen = new EventEmitter<Place>();
  }

  ngOnInit() {
  }

  whenPlaceThingsClicked() {
    this.placeChosen.emit(this.place);
  }
}
