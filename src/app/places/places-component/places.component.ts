import {Component, OnInit, EventEmitter, Input, Output} from '@angular/core';
import {Place} from '../Place';

@Component({
  selector: 'app-places',
  templateUrl: './places.component.html',
  styleUrls: ['./places.component.css']
})
export class PlacesComponent implements OnInit {
  @Input() places: Array<Place>;

  @Output() placeChosen: EventEmitter<Place>;

  constructor() {
    this.placeChosen = new EventEmitter<Place>();
  }

  ngOnInit() {
  }

  whenPlaceChosen(place: Place) {
    this.placeChosen.emit(place);
  }
}
