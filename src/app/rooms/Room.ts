export class Room {
  public name: string;

  constructor(name: string) {
    this.name = name;
  }
}
