import {Component, EventEmitter, OnInit, Input, Output} from '@angular/core';
import {Room} from '../Room';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {
  @Input() room: Room;

  @Output() roomChosen: EventEmitter<Room>;

  constructor() {
    this.roomChosen = new EventEmitter<Room>();
  }

  ngOnInit() {
  }

  public whenRoomPlacesClicked() {
    this.roomChosen.emit(this.room);
  }
}
