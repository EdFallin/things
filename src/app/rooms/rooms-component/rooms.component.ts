import {Component, OnInit, EventEmitter, Input, Output} from '@angular/core';
import {Room} from '../Room';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {
  @Input() rooms: Array<Room>;

  @Output() roomChosen: EventEmitter<Room>;

  constructor() {
    this.roomChosen = new EventEmitter<Room>();
  }

  ngOnInit() {
  }

  whenRoomChosen(room: Room) {
    this.roomChosen.emit(room);
  }

}
