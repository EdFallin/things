import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {RoomsComponent} from './rooms/rooms-component/rooms.component';
import {RoomComponent} from './rooms/room-component/room.component';
import {PlacesComponent} from './places/places-component/places.component';
import {PlaceComponent} from './places/place-component/place.component';
import {ThingsComponent} from './things/things-component/things.component';
import {ThingComponent} from './things/thing-component/thing.component';
import {ThingService} from './thing-service/thing.service';
import {NewThingComponent} from './things/new-thing-component/new-thing.component';

@NgModule({
  declarations: [
    AppComponent,
    RoomsComponent,
    RoomComponent,
    PlacesComponent,
    PlaceComponent,
    ThingsComponent,
    ThingComponent,
    NewThingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    ThingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
