import {Injectable} from '@angular/core';
import {Room} from '../rooms/Room';
import {Place} from '../places/Place';
import {Thing} from '../things/Thing';
import {ThingType} from '../things/ThingType';

import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';
import {of as ObservableOf} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThingService {
  serviceHttpRoot = 'https://thingswebapi.azurewebsites.net';

  constructor(private http: HttpClient) {
  }

  getRooms(): Observable<Room[]> {
    const url = this.serviceHttpRoot + '/source/getRooms';
    const rooms = this.http.get<Room[]>(url);
    return rooms;
  }

  getPlacesInRoom(room: Room): Observable<Place[]> {
    const url = this.serviceHttpRoot + '/source/getPlacesInRoom/room/' + room.name;
    const places = this.http.get<Place[]>(url);
    return places;
  }

  getThingsInPlaceInRoom(room: Room, place: Place): Observable<Thing[]> {
    const roomName = room.name;
    const placeName = place.name;

    const url = this.serviceHttpRoot
      + '/source/getThingsInPlaceInRoom'
      + '/room/' + roomName
      + '/place/' + placeName;

    const things = this.http.get<Thing[]>(url);
    return things;
  }

  addThingToPlaceInRoom(thing: Thing, place: Place, room: Room): Observable<boolean> {
    const roomName = room.name;
    const placeName = place.name;

    const url = this.serviceHttpRoot + '/source/addThingToPlaceInRoom';
    const body = {thing, place, room};

    const didSucceed = this.http.post<boolean>(url, body);
    return didSucceed;
  }
}
