import {Component, OnInit} from '@angular/core';
import {Input} from '@angular/core';
import {Thing} from '../Thing';

@Component({
  selector: 'app-thing',
  templateUrl: './thing.component.html',
  styleUrls: ['./thing.component.css']
})
export class ThingComponent implements OnInit {
  @Input() public thing: Thing;
  @Input() public index: number;

  constructor() {
  }

  ngOnInit() {
  }

}
