import {Component, OnInit, Input} from '@angular/core';
import {Thing} from '../Thing';
import {Room} from '../../rooms/Room';
import {Place} from '../../places/Place';
import {ThingService} from '../../thing-service/thing.service';

@Component({
  selector: 'app-things',
  templateUrl: './things.component.html',
  styleUrls: ['./things.component.css']
})
export class ThingsComponent implements OnInit {
  @Input() things: Array<Thing>;
  @Input() room: Room;
  @Input() place: Place;

  constructor(private thingService: ThingService) {
  }

  ngOnInit() {
  }

  whenThingsChanged() {
    this.thingService.getThingsInPlaceInRoom(this.room, this.place)
      .subscribe(things => {
        this.things = things;
      });
  }

}
