import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Thing} from '../Thing';
import {ThingService} from '../../thing-service/thing.service';
import {ThingType} from '../ThingType';
import {Room} from '../../rooms/Room';
import {Place} from '../../places/Place';

@Component({
  selector: 'app-new-thing',
  templateUrl: './new-thing.component.html',
  styleUrls: ['./new-thing.component.css']
})
export class NewThingComponent implements OnInit {
  public isAddingNow = false;
  public thing = new Thing('', ThingType.Unknown);
  public types: ThingType[];

  @Input() room: Room;
  @Input() place: Place;

  @Output() thingsChanged: EventEmitter<any>;

  constructor(private thingService: ThingService) {
    this.fillTypes();
    this.thingsChanged = new EventEmitter<any>();
  }

  fillTypes() {
    // this syntax uses static-like syntax with Object and the ThingType class and a traverse-by-index method,
    // with ''x in the lambda the index, and returns an array of whatever the lambda returns
    this.types = Object.keys(ThingType)
      .filter(x => typeof ThingType[x]) as ThingType[];
  }

  ngOnInit() {
  }

  whenPlusButtonClicked() {
    this.isAddingNow = true;
  }

  whenStoreButtonClicked(thingEntry) {
    if (thingEntry.valid) {
      this.thingService.addThingToPlaceInRoom(this.thing, this.place, this.room)
        .subscribe(didSucceed => {
          if (didSucceed) {
            this.clearEntry(thingEntry);
            this.thingsChanged.emit(null);
          } else {
            /* ++ more Angular / more robust handling needed */
            console.log('Unable to add thing ' + this.thing.name);
          }
        });
    }
  }

  whenCancelButtonClicked(thingEntry) {
    this.clearEntry(thingEntry);
  }

  private clearEntry(thingEntry) {
    thingEntry.reset();
    this.thing.type = ThingType.Unknown;  // since reset() makes this null
    this.isAddingNow = false;
  }
}
