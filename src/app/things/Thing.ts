import {ThingType} from './ThingType';

export class Thing {
  constructor(public name: string, public type: ThingType = ThingType.Unknown) {
    // no operations
  }
}
