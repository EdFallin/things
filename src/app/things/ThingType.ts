export enum ThingType {
  Book = 'Book',
  Supply = 'Supply',
  Clothing = 'Clothing',
  Food = 'Food',
  Cooking = 'Cooking',
  Tool = 'Tool',
  Video = 'Video',
  Music = 'Music',
  Memento = 'Memento',
  Other = 'Other',
  Unknown = 'Unknown'  // default
}
